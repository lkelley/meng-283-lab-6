#include <Arduino.h>
#include "ExerciseOne.h"
#include "ExerciseTwo.h"

ExerciseTwo exercise;

void setup() {
    exercise.setup();
}

void loop() {
    exercise.loop();
}
