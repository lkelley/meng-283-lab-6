#include <Arduino.h>
#include "ExerciseTwo.h"

void ExerciseTwo::setup() {
    pinMode(this->fanPin, OUTPUT);
    pinMode(13, OUTPUT);

    // Begin serial
    Serial.begin(9600);

    // Init fan pin off
    digitalWrite(this->fanPin, LOW);

    // Flash 13 rapidly for 4 seconds
    for (int i = 1; i <= 20; i++) {
        digitalWrite(13, HIGH);
        delay(100);
        digitalWrite(13, LOW);
        delay(100);
    }

    Serial.print("Input Voltage (mV)\t");
    Serial.println("Output PWM Value");
}

void ExerciseTwo::loop() {
    int input = analogRead(this->setPin);
    int inputVoltage = map(input, 0, 1023, 0, 5000);
    int pwmOutput = map(input, 0, 1023, 0, 255);

    Serial.print(inputVoltage);
    Serial.print("\t\t");
    Serial.println(pwmOutput);

    analogWrite(this->fanPin, pwmOutput);
}
