#include <Arduino.h>

class ExerciseTwo
{
    private:
        const short fanPin = 3;
        const short setPin = A0;

    public:
        void setup();
        void loop();
};