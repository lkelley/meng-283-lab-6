#include <Arduino.h>
#include "ExerciseOne.h"

void ExerciseOne::setup() {
    pinMode(this->pwmPin, OUTPUT);
    pinMode(13, OUTPUT);

    // Init PWM pin off
    digitalWrite(this->pwmPin, LOW);
    this->pwmVal = 0;

    // Start serial
    Serial.begin(9600);

    // Set PWM output to 127 duty cycle
    analogWrite(this->pwmPin, 127);

    // Flash 13 at 1 Hz for 10 seconds (measure during this period)
    for (int i = 1; i <= 10; i++) {
        digitalWrite(13, HIGH);
        delay(500);
        digitalWrite(13, LOW);
        delay(500);
    }

    digitalWrite(this->pwmPin, LOW);

    // Flash 13 rapidly for 4 seconds
    for (int i = 1; i <= 20; i++) {
        digitalWrite(13, HIGH);
        delay(100);
        digitalWrite(13, LOW);
        delay(100);
    }
}

// Increase PWM duty cycle from 0 to 250/255 by 50 every 5 seconds, resetting after 250
void ExerciseOne::loop() {
    analogWrite(this->pwmPin, this->pwmVal);

    Serial.print("Set to ");
    Serial.println(this->pwmVal);

    this->pwmVal = this->pwmVal >= 250 // Highest this loop will reach is 250
        ? 0 // Reset to 0
        : pwmVal + 25; // Increase by 25

    delay(5000);
}
